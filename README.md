<!-- AUTO-GENERATED-CONTENT:START (STARTER) -->
<p align="center">
  <a href="https://www.gatsbyjs.org">
    <img alt="Gatsby" src="https://www.gatsbyjs.org/monogram.svg" width="60" />
  </a>
</p>
<h1 align="center">
  Leaders of Excellency UNiversity
</h1>

<p> This project has been bootstraped with the Gatsby-cli</p>

<h4> How to run </h4>
1. <code>npm install</code>
2. <code>gatsby develop</code>

<p> You can find the live demo at [https://leadersofexcellency.netlify.com/](https://leadersofexcellency.netlify.com/)

<!-- AUTO-GENERATED-CONTENT:END -->
