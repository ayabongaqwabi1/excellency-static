import React from 'react';
import './style.scss';

export default () => {
    const courses = [
        {
            title: "Certificate in Ministry",
        },
        {
            title: "Deploma in Theology",
        },
        {
            title: "Bachelor Degree in Theology",
        },
        {
            title: "Bachelor Degree in Something",
        },
        {
            title: "Bachelor Degree in Ministry",
        },
        {
            title: "Bachelor Degree in Prophetic Ministry",
        },
        {
            title: "Bachelor Degree in Apostolic Ministry",
        },
        {
            title: "Bachelor Degree in Christian Counselling",
        }
    ]
    return (
        <div className="course-container">
            <div className='row'>
                <div className="course-label col-md-3 col-xs-12">
                    <h2>Courses</h2>
                </div>
                <div className="col-md-8 col-xs-12">
                    <div className="row courses">
                        {
                            courses.map(c => (
                                <div className="col-md-3 col-xs-6 course">
                                    <div className="course-details">
                                        <h4>{c.title}</h4>
                                    </div>
                                </div>
                            ))
                        }
                    </div>
                </div>
            </div>

        </div>
    )
}