import React, { Component } from "react";
import { push as Menu } from "react-burger-menu";
import './style.scss';

export default props => {
    return (
        <Menu right>
            <a className="menu-item" href="/">
                Home
            </a>
            <a className="menu-item" href="/burgers">
                About
            </a>
            <a className="menu-item" href="/pizzas">
                Blog
            </a>
            <a className="menu-item" href="/desserts">
                Your Work
            </a>
            <a className="menu-item" href="/desserts">
                Online Classes
            </a>
        </Menu>
    );
};