import React, { Component } from "react";
import "./style.scss";
import Layout from '../layout';
import logo from '../../static/logo.png';
import arrow from '../../static/point_down.svg';
import Courses from '../components/courses'
import { Element as ScrollElement, scroller } from 'react-scroll'
class Home extends Component {
    constructor(props) {
        super(props);
        this.state = { home: '' };
        this.scrollToDetails = this.scrollToDetails.bind(this)
    }

    scrollToDetails() {
        scroller.scrollTo('details', {
            duration: 800,
            delay: 0,
            smooth: 'easeInOutQuart',
        });
    }

    render() {
        return (
            <Layout>
                <div className="page-container">

                    <div className="overlay">
                        <div className="hero">
                        </div>
                        <div className='row text-center'>
                            <div className="col-md-8 orglogo text-center">
                                <img src={logo} />
                            </div>
                        </div>
                        <div className='row text-center'>
                            <div className="col-md-8 orgname">
                                <h1> Leaders Of Excellency University </h1>
                                <button className="btn btn-orange enroll-btn">Enroll Now </button>
                            </div>
                            <div className="col-md-12 col-xs-12 scroll-down">
                                <img className="arrow" src={arrow} onClick={this.scrollToDetails} />
                            </div>
                        </div>
                    </div>
                    <ScrollElement name="details">
                        <div className="details row">
                            <div className="faculty col-md-4 col-xs-12 deet">
                                <div className="info">
                                    <div className="image">
                                    </div>
                                    <div className="text">
                                        <p>
                                            Leaders Of Excellency’s esteemed faculty members are experts in an ever-widening array
                                            of disciplines and faith We’re commited in mentoring students.
                                        </p>
                                        <button> more </button>
                                    </div>
                                </div>
                            </div>
                            <div className="equipment col-md-4 col-xs-12 deet">
                                <div className="info">
                                    <div className="image">
                                    </div>
                                    <div className="text">
                                        <p>
                                            Leaders Of Excellency’s Institutes & Initiatives advance social movements and provide
                                            seminarians with unique opportunities.
                                        </p>
                                        <button> more </button>
                                    </div>
                                </div>
                            </div>
                            <div className="support col-md-4 col-xs-12 deet">
                                <div className="info">
                                    <div className="image">
                                    </div>
                                    <div className="text">
                                        <p>
                                            Leaders Of Excellency understands that true ministry begins when the ideas espoused in
                                            classrooms and sanctuaries collide with the public square.
                                        </p>
                                        <button> more </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </ScrollElement>
                    <Courses />
                </div>
            </Layout>
        )
    }
}
export default Home;
