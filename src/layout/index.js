import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Helmet from 'react-helmet'
import metaImage from '../../static/logo.png'
import SideBar from '../components/sidebar'
import 'bootstrap/dist/css/bootstrap.min.css';

class Layout extends Component {
    render() {
        return (
            <div>
                <Helmet
                    title={"Leaders of Excellency University"}
                    meta={[
                        { name: 'description', content: ' We have the experience to complete even the most difficult projects with courteous professional service yielding the highest quality finished product' },
                        { name: 'keywords', content: 'construction,  paving, carpentry, commercial cleaning' },
                        { name: 'og:image', content: metaImage },
                    ]}
                >
                    <html lang="en" />
                    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,300,700' rel='stylesheet' type='text/css' />
                    <meta name="google-site-verification" content="c_vopkaZKBV00C6zDFQonGXA-5tgfSJDOKsKTGtDmEk" />
                    <meta name='viewport' content="width=device-width, initial-scale=1" />
                    <script type="text/javascript" src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
                </Helmet>
                <div id="App">
                    <SideBar pageWrapId={"page-wrap"} outerContainerId={"App"} />
                    <div id="page-wrap">
                        {this.props.children}
                    </div>
                </div>
            </div>
        )
    }
}


export default Layout
