module.exports = {
    siteMetadata: {
        title: 'Leaders of Excellency University',
        siteUrl: "https://www.leadersofexcellency.co.za"
    },
    plugins: [
        'gatsby-plugin-sitemap',
        'gatsby-plugin-sass',
        'gatsby-plugin-react-helmet'
    ],
}
